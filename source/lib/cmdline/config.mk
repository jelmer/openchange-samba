[SUBSYSTEM::LIBCMDLINE_CREDENTIALS]
PRIVATE_PROTO_HEADER = credentials.h
OBJ_FILES = credentials.o
PUBLIC_DEPENDENCIES = CREDENTIALS LIBPOPT

[SUBSYSTEM::POPT_SAMBA]
OBJ_FILES = popt_common.o
PUBLIC_DEPENDENCIES = LIBPOPT

PUBLIC_HEADERS += lib/cmdline/popt_common.h 

[SUBSYSTEM::POPT_CREDENTIALS]
PRIVATE_PROTO_HEADER = popt_credentials.h
OBJ_FILES = popt_credentials.o
PUBLIC_DEPENDENCIES = CREDENTIALS LIBCMDLINE_CREDENTIALS LIBPOPT
PRIVATE_DEPENDENCIES = LIBSAMBA-UTIL
