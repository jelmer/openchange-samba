[SUBSYSTEM::LIBCLI_LDAP]
PRIVATE_PROTO_HEADER = ldap_proto.h
OBJ_FILES = ldap.o \
		ldap_client.o \
		ldap_bind.o \
		ldap_msg.o \
		ldap_ildap.o \
		ldap_controls.o
PUBLIC_DEPENDENCIES = LIBSAMBA-ERRORS LIBEVENTS LIBPACKET 
PRIVATE_DEPENDENCIES = LIBCLI_COMPOSITE samba-socket NDR_SAMR LIBTLS ASN1_UTIL \
					   LDAP_ENCODE LIBNDR LP_RESOLVE gensec

PUBLIC_HEADERS += libcli/ldap/ldap.h libcli/ldap/ldap_ndr.h

[SUBSYSTEM::LDAP_ENCODE]
OBJ_FILES = ldap_ndr.o
# FIXME PRIVATE_DEPENDENCIES = LIBLDB
