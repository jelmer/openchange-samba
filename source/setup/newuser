#!/usr/bin/python
#
#	add a new user to a Samba4 server
#	Copyright Andrew Tridgell 2005
#	Copyright Jelmer Vernooij 2008
#	Released under the GNU GPL version 3 or later
#

import samba.getopt as options
import optparse
import pwd
import sys
from getpass import getpass
from auth import system_session
from samba.samdb import SamDB

parser = optparse.OptionParser("newuser [options] <username> [<password>]")
sambaopts = options.SambaOptions(parser)
parser.add_option_group(sambaopts)
parser.add_option_group(options.VersionOptions(parser))
credopts = options.CredentialsOptions(parser)
parser.add_option_group(credopts)
parser.add_option("--quiet", help="Be quiet", action="store_true")
parser.add_option("--unixname", help="Unix Username", type=str)

opts, args = parser.parse_args()

#
#  print a message if quiet is not set
#
def message(text):
	if not opts.quiet:
		print text

if len(args) == 0:
	parser.print_usage()
	sys.exit(1)

username = args[0]
if len(args) > 1:
	password = args[1]
else:
	password = getpass("New Password: ")

if opts.unixname is None:
	opts.unixname = username

try:
	pwd.getpwnam(opts.unixname)
except KeyError:
	print "ERROR: Unix user '%s' does not exist" % opts.unixname
	sys.exit(1)

creds = credopts.get_credentials()

lp = sambaopts.get_loadparm()
samdb = SamDB(url=lp.get("sam database"), session_info=system_session(), 
              credentials=creds, lp=lp)
samdb.newuser(username, opts.unixname, password)
