<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//Samba-Team//DTD DocBook V4.2-Based Variant V1.0//EN" "http://www.samba.org/samba/DTD/samba-doc">
<refentry id="ctdb.1">

<refmeta>
	<refentrytitle>ctdb</refentrytitle>
	<manvolnum>1</manvolnum>
</refmeta>


<refnamediv>
	<refname>ctdb</refname>
        <refpurpose>clustered tdb database management utility</refpurpose>
</refnamediv>

<refsynopsisdiv>
	<cmdsynopsis>
		<command>ctdb [ OPTIONS ] COMMAND ...</command>
	</cmdsynopsis>
	
	<cmdsynopsis>
		<command>ctdb</command>
		<arg choice="opt">-n &lt;node&gt;</arg>
		<arg choice="opt">-Y</arg>
		<arg choice="opt">-t &lt;timeout&gt;</arg>
		<arg choice="opt">-? --help</arg>
		<arg choice="opt">--usage</arg>
		<arg choice="opt">-d --debug=&lt;INTEGER&gt;</arg>
		<arg choice="opt">--socket=&lt;filename&gt;</arg>
	</cmdsynopsis>
	
</refsynopsisdiv>

  <refsect1><title>DESCRIPTION</title>
    <para>
      ctdb is a utility to view and manage a ctdb cluster.
    </para>
  </refsect1>


  <refsect1>
    <title>OPTIONS</title>

    <variablelist>
      <varlistentry><term>-n &lt;node&gt;</term>
        <listitem>
          <para>
            This specifies on which node to execute the command. Default is
            to run the command on the deamon running on the local host.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>-Y</term>
        <listitem>
          <para>
            Produce output in machinereadable form for easier parsing by scripts. Not all commands support this option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>-t &lt;timeout&gt;</term>
        <listitem>
          <para>
            How long should ctdb wait for a command to complete before timing out. Default is 3 seconds.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>-? --help</term>
        <listitem>
          <para>
            Print some help text to the screen.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--usage</term>
        <listitem>
          <para>
            Print useage information to the screen.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>-d --debug=&lt;debuglevel&gt;</term>
        <listitem>
          <para>
            Change the debug level for the command. Default is 0.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--socket=&lt;filename&gt;</term>
        <listitem>
          <para>
            Specify the socketname to use when connecting to the local ctdb 
            daemon. The default is /tmp/ctdb.socket .
          </para>
          <para>
            You only need to specify this parameter if you run multiple ctdb 
            daemons on the same physical host and thus can not use the default
            name for the domain socket.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
  </refsect1>


  <refsect1><title>Administrative Commands</title>
    <para>
      These are commands used to monitor and administrate a CTDB cluster.
    </para>

    <refsect2><title>status</title>
      <para>
        This command shows the current status of the ctdb node.
      </para>

      <refsect3><title>node status</title>
        <para>
          Node status reflects the current status of the node. There are four possible states:
        </para>
        <para>
          OK - This node is fully functional.
        </para>
        <para>
          DISCONNECTED - This node could not be connected through the network and is currently not parcipitating in the cluster. If there is a public IP address associated with this node it should have been taken over by a different node. No services are running on this node.
        </para>
        <para>
          DISABLED - This node has been administratively disabled. This node is still functional and participates in the CTDB cluster but its IP addresses have been taken over by a different node and no services are currently being hosted.
        </para>
        <para>
          UNHEALTHY - A service provided by this node is malfunctioning and should be investigated. The CTDB daemon itself is operational and participates in the cluster. Its public IP address has been taken over by a different node and no services are currnetly being hosted. All unhealthy nodes should be investigated and require an administrative action to rectify.
        </para>
        <para>
          BANNED - This node failed too many recovery attempts and has been banned from participating in the cluster for a period of RecoveryBanPeriod seconds. Any public IP address has been taken over by other nodes. This node does not provide any services. All banned nodes should be investigated and require an administrative action to rectify. This node does not perticipate in the CTDB cluster but can still be communicated with. I.e. ctdb commands can be sent to it.
        </para>
      </refsect3>

      <refsect3><title>generation</title>
        <para>
          The generation id is a number that indicates the current generation 
          of a cluster instance. Each time a cluster goes through a 
          reconfiguration or a recovery its generation id will be changed.
        </para>
      </refsect3>

      <refsect3><title>VNNMAP</title>
        <para>
          The list of Virtual Node Numbers. This is a list of all nodes that actively participates in the cluster and that share the workload of hosting the Clustered TDB database records.
          Only nodes that are parcipitating in the vnnmap can become lmaster or dmaster for a database record.
        </para>
      </refsect3>

      <refsect3><title>Recovery mode</title>
        <para>
          This is the current recovery mode of the cluster. There are two possible modes:
        </para>
        <para>
          NORMAL - The cluster is fully operational.
        </para>
        <para>
          RECOVERY - The cluster databases have all been frozen, pausing all services while the cluster awaits a recovery process to complete. A recovery process should finish within seconds. If a cluster is stuck in the RECOVERY state this would indicate a cluster malfunction which needs to be investigated.
        </para>
      </refsect3>

      <refsect3><title>Recovery master</title>
        <para>
          This is the cluster node that is currently designated as the recovery master. This node is responsible of monitoring the consistency of the cluster and to perform the actual recovery process when reqired.
        </para>
      </refsect3>

      <para>
	Example: ctdb status
      </para>
      <para>Example output:</para>
      <screen format="linespecific">
Number of nodes:4
vnn:0 11.1.2.200       OK (THIS NODE)
vnn:1 11.1.2.201       OK
vnn:2 11.1.2.202       OK
vnn:3 11.1.2.203       OK
Generation:1362079228
Size:4
hash:0 lmaster:0
hash:1 lmaster:1
hash:2 lmaster:2
hash:3 lmaster:3
Recovery mode:NORMAL (0)
Recovery master:0
      </screen>
    </refsect2>

    <refsect2><title>ping</title>
      <para>
        This command will "ping" all CTDB daemons in the cluster to verify that they are processing commands correctly.
      </para>
      <para>
	Example: ctdb ping
      </para>
      <para>
	Example output:
      </para>
      <screen format="linespecific">
response from 0 time=0.000054 sec  (3 clients)
response from 1 time=0.000144 sec  (2 clients)
response from 2 time=0.000105 sec  (2 clients)
response from 3 time=0.000114 sec  (2 clients)
      </screen>
    </refsect2>

    <refsect2><title>ip</title>
      <para>
        This command will display the list of public addresses that are provided by the cluster and which physical node is currently serving this ip.
      </para>
      <para>
	Example: ctdb ip
      </para>
      <para>
	Example output:
      </para>
      <screen format="linespecific">
Number of nodes:4
12.1.1.1         0
12.1.1.2         1
12.1.1.3         2
12.1.1.4         3
      </screen>
    </refsect2>

    <refsect2><title>getvar &lt;name&gt;</title>
      <para>
        Get the runtime value of a tuneable variable.
      </para>
      <para>
	Example: ctdb getvar MaxRedirectCount
      </para>
      <para>
	Example output:
      </para>
      <screen format="linespecific">
MaxRedirectCount    = 3
      </screen>
    </refsect2>

    <refsect2><title>setvar &lt;name&gt; &lt;value&gt;</title>
      <para>
        Set the runtime value of a tuneable variable.
      </para>
      <para>
	Example: ctdb setvar MaxRedirectCount 5
      </para>
    </refsect2>

    <refsect2><title>listvars</title>
      <para>
        List all tuneable variables.
      </para>
      <para>
	Example: ctdb listvars
      </para>
      <para>
	Example output:
      </para>
      <screen format="linespecific">
MaxRedirectCount    = 5
SeqnumFrequency     = 1
ControlTimeout      = 60
TraverseTimeout     = 20
KeepaliveInterval   = 2
KeepaliveLimit      = 3
MaxLACount          = 7
RecoverTimeout      = 5
RecoverInterval     = 1
ElectionTimeout     = 3
TakeoverTimeout     = 5
MonitorInterval     = 15
EventScriptTimeout  = 20
RecoveryGracePeriod = 60
RecoveryBanPeriod   = 300
      </screen>
    </refsect2>

    <refsect2><title>statistics</title>
      <para>
        Collect statistics from the CTDB daemon about how many calls it has served.
      </para>
      <para>
	Example: ctdb statistics
      </para>
      <para>
	Example output:
      </para>
      <screen format="linespecific">
CTDB version 1
 num_clients                        3
 frozen                             0
 recovering                         0
 client_packets_sent           360489
 client_packets_recv           360466
 node_packets_sent             480931
 node_packets_recv             240120
 keepalive_packets_sent             4
 keepalive_packets_recv             3
 node
     req_call                       2
     reply_call                     2
     req_dmaster                    0
     reply_dmaster                  0
     reply_error                    0
     req_message                   42
     req_control               120408
     reply_control             360439
 client
     req_call                       2
     req_message                   24
     req_control               360440
 timeouts
     call                           0
     control                        0
     traverse                       0
 total_calls                        2
 pending_calls                      0
 lockwait_calls                     0
 pending_lockwait_calls             0
 memory_used                     5040
 max_hop_count                      0
 max_call_latency                   4.948321 sec
 max_lockwait_latency               0.000000 sec
      </screen>
    </refsect2>

    <refsect2><title>statisticsreset</title>
      <para>
        This command is used to clear all statistics counters in a node.
      </para>
      <para>
	Example: ctdb statisticsreset
      </para>
    </refsect2>

    <refsect2><title>getdebug</title>
      <para>
        Get the current debug level for the node. the debug level controls what information is written to the log file.
      </para>
    </refsect2>

    <refsect2><title>setdebug &lt;debuglevel&gt;</title>
      <para>
        Set the debug level of a node. This is a number between 0 and 9 and controls what information will be written to the logfile.
      </para>
    </refsect2>

    <refsect2><title>getpid</title>
      <para>
        This command will return the process id of the ctdb daemon.
      </para>
    </refsect2>

    <refsect2><title>disable</title>
      <para>
        This command is used to administratively disable a node in the cluster.
        A disabled node will still participate in the cluster and host
        clustered TDB records but its public ip address has been taken over by
        a different node and it no longer hosts any services.
      </para>
    </refsect2>

    <refsect2><title>enable</title>
      <para>
        Re-enable a node that has been administratively disabled.
      </para>
    </refsect2>

    <refsect2><title>ban &lt;bantime|0&gt;</title>
      <para>
        Administratively ban a node for bantime seconds. A bantime of 0 means that the node should be permanently banned. 
      </para>
      <para>
        A banned node does not participate in the cluster and does not host any records for the clustered TDB. Its ip address has been taken over by an other node and no services are hosted.
      </para>
      <para>
        Nodes are automatically banned if they are the cause of too many
        cluster recoveries.
      </para>
    </refsect2>

    <refsect2><title>unban</title>
      <para>
        This command is used to unban a node that has either been 
        administratively banned using the ban command or has been automatically
        banned by the recovery daemon.
      </para>
    </refsect2>

    <refsect2><title>shutdown</title>
      <para>
        This command will shutdown a specific CTDB daemon.
      </para>
    </refsect2>

    <refsect2><title>recover</title>
      <para>
        This command will trigger the recovery daemon to do a cluster
        recovery.
      </para>
    </refsect2>

    <refsect2><title>killtcp &lt;srcip:port&gt; &lt;dstip:port&gt;</title>
      <para>
        This command will kill the specified TCP connection by issuing a
        TCP RST to the srcip:port endpoint.
      </para>
    </refsect2>

    <refsect2><title>tickle &lt;srcip:port&gt; &lt;dstip:port&gt;</title>
      <para>
        This command will will send a TCP tickle to the source host for the
        specified TCP connection.
	A TCP tickle is a TCP ACK packet with an invalid sequence and 
	acknowledge number and will when received by the source host result
        in it sending an immediate correct ACK back to the other end.
      </para>
      <para>
        TCP tickles are useful to "tickle" clients after a IP failover has 
        occured since this will make the client immediately recognize the 
        TCP connection has been disrupted and that the client will need
        to reestablish. This greatly speeds up the time it takes for a client
        to detect and reestablish after an IP failover in the ctdb cluster.
      </para>
    </refsect2>

  </refsect1>


  <refsect1><title>Debugging Commands</title>
    <para>
      These commands are primarily used for CTDB development and testing and
      should not be used for normal administration.
    </para>
    <refsect2><title>process-exists &lt;pid&gt;</title>
      <para>
        This command checks if a specific process exists on the CTDB host. This is mainly used by Samba to check if remote instances of samba are still running or not.
      </para>
    </refsect2>

    <refsect2><title>getdbmap</title>
      <para>
        This command lists all clustered TDB databases that the CTDB daemon has attahced to.
      </para>
      <para>
	Example: ctdb getdbmap
      </para>
      <para>
	Example output:
      </para>
      <screen format="linespecific">
Number of databases:4
dbid:0x42fe72c5 name:locking.tdb path:/var/ctdb/locking.tdb.0
dbid:0x1421fb78 name:brlock.tdb path:/var/ctdb/brlock.tdb.0
dbid:0x17055d90 name:connections.tdb path:/var/ctdb/connections.tdb.0
dbid:0xc0bdde6a name:sessionid.tdb path:/var/ctdb/sessionid.tdb.0
      </screen>
    </refsect2>

    <refsect2><title>catdb &lt;dbname&gt;</title>
      <para>
        This command will dump a clustered TDB database to the screen. This is a debugging command.
      </para>
    </refsect2>

    <refsect2><title>getmonmode</title>
      <para>
        This command returns the monutoring mode of a node. The monitoring mode is either ACTIVE or DISABLED. Normally a node will continously monitor that all other nodes that are expected are in fact connected and that they respond to commands.
      </para>
      <para>
        ACTIVE - This is the normal mode. The node is actively monitoring all other nodes, both that the transport is connected and also that the node responds to commands. If a node becomes unavailable, it will be marked as DISCONNECTED and a recovery is initiated to restore the cluster.
      </para>
      <para>
        DISABLED - This node is not monitoring that other nodes are available. In this mode a node failure will not be detected and no recovery will be performed. This mode is useful when for debugging purposes one wants to attach GDB to a ctdb process but wants to prevent the rest of the cluster from marking this node as DISCONNECTED and do a recovery.
      </para>
    </refsect2>


    <refsect2><title>setmonmode &lt;0|1&gt;</title>
      <para>
        This command can be used to explicitely disable/enable monitoring mode on a node. The main purpose is if one wants to attach GDB to a running ctdb daemon but wants to prevent the other nodes from marking it as DISCONNECTED and issuing a recovery. To do this, set monitoring mode to 0 on all nodes before attaching with GDB. Remember to set monitoring mode back to 1 afterwards.
      </para>
    </refsect2>

    <refsect2><title>attach &lt;dbname&gt;</title>
      <para>
        This is a debugging command. This command will make the CTDB daemon create a new CTDB database and attach to it.
      </para>
    </refsect2>

    <refsect2><title>dumpmemory</title>
      <para>
        This is a debugging command. This command will make the ctdb daemon to write a fill memory allocation map to the log file.
      </para>
    </refsect2>

    <refsect2><title>freeze</title>
      <para>
        This command will lock all the local TDB databases causing clients 
        that are accessing these TDBs such as samba3 to block until the
        databases are thawed.
      </para>
      <para>
        This is primarily used by the recovery daemon to stop all samba
        daemons from accessing any databases while the database is recovered
        and rebuilt.
      </para>
    </refsect2>

    <refsect2><title>thaw</title>
      <para>
        Thaw a previously frozen node.
      </para>
    </refsect2>

  </refsect1>


  <refsect1><title>SEE ALSO</title>
    <para>
      ctdbd(1), onnode(1)
      <ulink url="http://ctdb.samba.org/"/>
    </para>
  </refsect1>
  <refsect1><title>COPYRIGHT/LICENSE</title>
<literallayout>
Copyright (C) Andrew Tridgell 2007
Copyright (C) Ronnie sahlberg 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see http://www.gnu.org/licenses/.
</literallayout>
  </refsect1>
</refentry>
