<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//Samba-Team//DTD DocBook V4.2-Based Variant V1.0//EN" "http://www.samba.org/samba/DTD/samba-doc">
<refentry id="onnode.1">

<refmeta>
	<refentrytitle>onnode</refentrytitle>
	<manvolnum>1</manvolnum>
</refmeta>


<refnamediv>
	<refname>onnode</refname>
        <refpurpose>run commands on ctdb nodes</refpurpose>
</refnamediv>

<refsynopsisdiv>
	<cmdsynopsis>
		<command>onnode NODE COMMAND ...</command>
	</cmdsynopsis>
</refsynopsisdiv>

  <refsect1><title>DESCRIPTION</title>
    <para>
      onnode is a utility to run commands on a specific node of a CTDB
      cluster, or on all nodes.
    </para>
    <para>
      The NODE option specifies which node to run a command on. You
      can specify a numeric node number (from 0 to N-1) or the special
      node 'all'.
    </para>
    <para>
      The COMMAND can be any shell command. The onnode utility uses
      ssh or rsh to connect to the remote nodes and run the command.
      </para>
    <para>
      If the COMMAND starts with a /, then the command is run via the
      'at' service. Otherwise the command is run in the foreground.
    </para>
  </refsect1>

  <refsect1><title>EXAMPLES</title>
    <para>
      The following command would show the process ID of ctdb on all nodes
      </para>
      <screen format="linespecific">
onnode all pidof ctdbd
      </screen>

    <para>
      The following command would show the last 5 lines of log on each
      node, preceded by the nodes hostname
      </para>
      <screen format="linespecific">
onnode all "hostname; tail -5 /var/log/log.ctdb"
      </screen>

    <para>
      The following command would restart the ctdb service on all nodes.
      </para>
      <screen format="linespecific">
onnode all service ctdb restart
      </screen>

    </refsect1>

  <refsect1><title>SEE ALSO</title>
    <para>
      ctdbd(1), ctdb(1), <ulink url="http://ctdb.samba.org/"/>
    </para>
  </refsect1>
  <refsect1><title>COPYRIGHT/LICENSE</title>
<literallayout>
Copyright (C) Andrew Tridgell 2007
Copyright (C) Ronnie sahlberg 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see http://www.gnu.org/licenses/.
</literallayout>
  </refsect1>
</refentry>
